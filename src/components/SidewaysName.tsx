import * as React from 'react';
import './SidewaysName.scss';

export interface SidewaysNameProps { name: string; }

export class SidewaysName extends React.PureComponent<SidewaysNameProps, {}> {
  render() {
    return (
      <h1 className="sideways-name">{this.props.name}</h1>
    );
  }
}
