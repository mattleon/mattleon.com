import * as React from 'react';
import * as ReactDOM from 'react-dom';

import './App.scss';

import { SidewaysName } from './components/SidewaysName';

class App extends React.PureComponent<{}, {}> {
  render() {
    return (
      <SidewaysName name="Matthew Leon" />
    );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));
